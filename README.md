# Vue Avatar Component

> Avatar component built with Vue 2

## Build Setup

``` bash
# install dependencies
npm run init

# serve with hot reload at localhost:8080
npm run dev

# lint the Typescript
npm run lint

# create a production build
npm run build

# deploy to firebase
npm run deploy

# serve dist directory with http-server
npm run serve
```
