import Vue from "vue";
import {Component, Prop} from "vue-property-decorator";
import "./avatar.styl";
const template = require("./avatar.vue");

export let bgColorsArray = ["#00FF00"];

export function setAvatarBgColors(colors: string[]) {
  bgColorsArray = colors;
}

@Component({
  template: template
})
export default class CircleAvatarComponent extends Vue {

  @Prop()
  avatar: string;
  @Prop({ default: "sm" })
  size: string;
  @Prop()
  profile: {firstName: string, lastName: string};
  @Prop()
  initials: string;

  style: {} = null;

  created(): void {
    // background section
    if (this.getAvatar) {
      this.style = {"background-image": "url(" + this.getAvatar + ")", "background-color": "#fff"};

    } else if (bgColorsArray && bgColorsArray.length > 0) {

      console.warn("component created.", bgColorsArray);
      const randomIndex = Math.floor(Math.random() * bgColorsArray.length);
      this.style = { "background-color": bgColorsArray[randomIndex] };

    }

    // text section
    if (this.initials && this.initials.trim() !== "") {

    }
  }

  get getAvatar(): string {
    return this.avatar;
  }

  static setDefaultSizes() {

  }

}