import * as chai from "chai";
import * as avoriaz from "avoriaz";
import CircleAvatarComponent, {setAvatarBgColors} from "../../src/avatar/avatar";

describe("avatar image tests", () => {

  let $;

  before(() => $ = require("jquery"));

  it("should display image on avatar ", () => {
    const imageUrl = "https://www.cat-image.png";

    let avatar = avoriaz.mount(CircleAvatarComponent);
    avatar.setProps({ avatar: imageUrl });

    chai.expect(avatar.getProp("avatar")).to.equal(imageUrl);
    const innerAvatar = avatar.find("._avatar > div.inner-avatar")[0];
    chai.expect(innerAvatar.hasStyle("background-image", imageUrl));
  });

  it("should set and apply default colors", () => {

    let colorsArray = ["#f12", "#aaa", "#ba1"];
    setAvatarBgColors(colorsArray);

    let avatar = avoriaz.mount(CircleAvatarComponent);

    const innerAvatar = avatar.find("._avatar > div.inner-avatar")[0];

    colorsArray.forEach(color => {
      console.warn(color, innerAvatar.hasStyle("background-color", color));
    });

  });

});
