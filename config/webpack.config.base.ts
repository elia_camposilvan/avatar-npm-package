import * as helpers from "./helpers";
import * as webpack from "webpack";

export const config: webpack.Configuration = {
  entry: {
    app: helpers.root("/src/avatar/avatar.ts")
  },
  output: {
    filename: "./dist/bundle.js",
    library: "circleAvatarComponent",
    libraryTarget: "umd"
  },
  devtool: "source-map",
  resolve: {
    extensions: [".ts", ".js", ".html"],
    alias: {
      mixins: helpers.root("/src/style/mixins.styl"),
      variables: helpers.root("/src/style/variables.styl"),
      $assets: helpers.root("/assets"),
      vue$: "vue/dist/vue.common.js",
      normalize: "normalize.css/normalize.css"
    }
  },
  module: {
    rules: [
      {test: /\.ts$/, include: /src/, enforce: "pre", loader: "tslint-loader"},
      {test: /\.ts$/, include: /src/, loader: "awesome-typescript-loader", options: {configFileName: "tsconfig.build.json"}},
      {test: /\.styl$/, include: /src/, loader: "style-loader!css-loader!stylus-loader"},
      {test: /\.vue$/,  include: /src/, loader: "raw-loader"},
      {test: /\.vue$/,  include: /node_modules/, loader: "vue-loader"},
      {test: /\.(ttf|png|bmp|jpeg|jpg|gif|svg|ico)([\?]?.*)$/, loader: "file-loader?name=[path][name].[ext]"}
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      "process.env": {
        ENV: JSON.stringify(process.env.NODE_ENV),
        NODE_ENV: JSON.stringify(process.env.NODE_ENV)
      }
    })
  ]
};
