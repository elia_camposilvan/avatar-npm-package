import {config} from "./webpack.config.base";

config.devServer = {
  port: 8090,
  host: "localhost",
  watchOptions: {aggregateTimeout: 300, poll: 1000},
  contentBase: './src',
  historyApiFallback: true
};

config.module = {
  rules: [
    {test: /\.ts$/, include: /src | test/, enforce: "pre", loader: "tslint-loader"},
    {test: /\.ts$/, include: /src | test/, loader: "awesome-typescript-loader", options: {configFileName: "tsconfig.build.json"}},
    {test: /\.styl$/, include: /src | test/, loader: "style-loader!css-loader!stylus-loader"},
    {test: /\.vue$/,  include: /src | test/, loader: "raw-loader"},
    {test: /\.vue$/,  include: /node_modules/, loader: "vue-loader"},
    {test: /index.html$/, exclude: /node_modules | test/, loader: 'file-loader?name=[name].[ext]'},
    {test: /\.(ttf|png|bmp|jpeg|jpg|gif|svg|ico)([\?]?.*)$/, loader: "file-loader?name=[path][name].[ext]"}
  ]
};

export default config;