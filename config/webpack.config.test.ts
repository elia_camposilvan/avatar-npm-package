import * as webpack from "webpack";
import * as helpers from "./helpers";

let config: webpack.Configuration = {
  output: {
    filename: "./dist/bundle.js",
    library: "circleAvatarComponent",
    libraryTarget: "umd"
  },
  devtool: "source-map",
  resolve: {
    extensions: [".ts", ".js", ".html"],
    alias: {
      mixins: helpers.root("/src/style/mixins.styl"),
      variables: helpers.root("/src/style/variables.styl"),
      $assets: helpers.root("/assets"),
      vue$: "vue/dist/vue.common.js",
      normalize: "normalize.css/normalize.css"
    }
  },
  module: {
    rules: [
      {test: /\.ts$/, exclude: /src | test/, enforce: "pre", loader: "tslint-loader"},
      {test: /\.ts$/, exclude: /src | test/, loader: "awesome-typescript-loader", options: {configFileName: "tsconfig.build.json"}},
      {test: /\.styl$/, include: /src/, loader: "style-loader!css-loader!stylus-loader"},
      {test: /\.vue$/,  include: /src/, loader: "raw-loader"},
      {test: /\.vue$/,  include: /node_modules/, loader: "vue-loader"},
      {test: /index.html$/, exclude: /node_modules | test/, loader: 'file-loader?name=[name].[ext]'},
      {test: /\.(ttf|png|bmp|jpeg|jpg|gif|svg|ico)([\?]?.*)$/, loader: "file-loader?name=[path][name].[ext]"}
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      "process.env": {
        ENV: JSON.stringify(process.env.NODE_ENV),
        NODE_ENV: JSON.stringify(process.env.NODE_ENV)
      }
    })
  ]
};

module.exports = config;
