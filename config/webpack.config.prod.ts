import UglifyJsPlugin from "webpack/lib/optimize/UglifyJsPlugin";
import CompressionPlugin from "compression-webpack-plugin";
import DefinePlugin from "webpack/lib/DefinePlugin";
import {config} from "./webpack.config.base";
import helpers from "./helpers";

config.entry["app.min"] = helpers.root("/src/app.ts");

config.plugins = [...config.plugins,
  new UglifyJsPlugin({
    include: /\.min\.js$/,
    minimize: true
  }),
  new CompressionPlugin({
    asset: "[path].gz[query]",
    test: /\.min\.js$/
  }),
  new DefinePlugin({
    "process.env": {
      ENV: JSON.stringify(process.env.NODE_ENV),
      NODE_ENV: JSON.stringify(process.env.NODE_ENV)
    }
  })
];

export default config;